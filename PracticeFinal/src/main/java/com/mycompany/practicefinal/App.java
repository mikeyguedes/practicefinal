package com.mycompany.practicefinal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * JavaFX App
 */
public class App extends Application {

    //run httpurlconnection
    private static void runHTTPURLConn() {

        //httpurlconnection code
        String website = "https://toolbox.googleapps.com/apps/dig/#A/";

        String str = "?q=cbc.ca";
        String response;

        response = HttpURLConnectionFinal.sendQuery(website, str);

        System.out.println("Response from the server: \n" + response);
    }

    private static void runAESEncryption() throws NoSuchAlgorithmException {
        var key = AESEncryptionFinal.GenerateKey(256);

        byte[] iv = AESEncryptionFinal.generateGMIV();

        String algorithm = "AES/GCM/NoPadding";

        String input = "Text to be encrypted";
        System.out.println("Plaintext: " + input);

        try {
            String cipherText = AESEncryptionFinal.encryptStr(algorithm, input, key, iv);
            System.out.println("CipherText: " + cipherText);

            String plainText = AESEncryptionFinal.decryptStr(algorithm, cipherText, key, iv);
            System.out.println("Decrypted text: " + plainText);

        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (InvalidAlgorithmParameterException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        }
    }

    //javafx code
    private Parent createContent() {
        return new StackPane(new Text("Hello World"));
    }

    @Override
    public void start(Stage stage) {
        stage.setScene(new Scene(createContent(), 400, 400));
        stage.show();
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {

        runHTTPURLConn();

        runAESEncryption();

        //javafx code
        launch();
    }

}
