/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.practicefinal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author mikeyguedes
 */
public class HttpURLConnectionFinal {

    //httpurlconnection code
    public static String sendQuery(String website, String params){

        StringBuilder data = new StringBuilder();

        try {
            URL url = new URL(website + params);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inStream = conn.getInputStream();

                BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
                String line;

                while ((line = in.readLine()) != null) {
                    data.append("\n" + line);
                }

                in.close();

            } else {
                System.err.println("Wrong response code: " + responseCode);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return data.toString();
    }
}
