/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.practicefinal;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

/**
 *
 * @author mikeyguedes
 */
public class AESEncryptionFinal {
    //set parameters
    public static final int GCM_IV_LENGTH = 12;
    public static final int GCM_TAG_LENGTH = 16;
    
    //generate key
    public static SecretKey GenerateKey(int n) throws NoSuchAlgorithmException{
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(n);
        
        return keyGen.generateKey();
    }
    
    //generate gcmiv
    public static byte[] generateGMIV(){
        byte[] GCMIV = new byte[GCM_IV_LENGTH];
        SecureRandom random = new SecureRandom();
        random.nextBytes(GCMIV);
        return GCMIV;
    }
    
    //encrypt a string
    public static String encryptStr(String algo, String text, SecretKey key, byte[] iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException{
        Cipher cipher = Cipher.getInstance(algo);
        
        GCMParameterSpec gcmParam = new GCMParameterSpec(GCM_TAG_LENGTH *8, iv); 
        
        cipher.init(Cipher.ENCRYPT_MODE, key, gcmParam);
        
        byte[] cipherText = cipher.doFinal(text.getBytes());
        
        return Base64.getEncoder().encodeToString(cipherText);
    }
    
    //decrypt a string
    public static String decryptStr(String algo, String cipherText, SecretKey key, byte[] iv) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
        Cipher cipher = Cipher.getInstance(algo);
        
        GCMParameterSpec gcmParam = new GCMParameterSpec(GCM_TAG_LENGTH *8, iv);
        cipher.init(Cipher.DECRYPT_MODE, key, gcmParam);
        
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
        
        return new String(plainText);
    }
    
    
}

